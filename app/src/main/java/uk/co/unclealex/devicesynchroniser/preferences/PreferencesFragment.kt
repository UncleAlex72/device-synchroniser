package uk.co.unclealex.devicesynchroniser.preferences

import android.content.SharedPreferences
import android.os.Bundle
import androidx.preference.EditTextPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceManager
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.Completable
import uk.co.unclealex.devicesynchroniser.R
import uk.co.unclealex.devicesynchroniser.preferences.Keys.KEY_CLEAR_DATABASES
import uk.co.unclealex.devicesynchroniser.preferences.Keys.KEY_DEVICE_IDENTIFIER
import uk.co.unclealex.devicesynchroniser.preferences.Keys.KEY_SERVER_URL
import uk.co.unclealex.devicesynchroniser.repository.DeviceRepository
import uk.co.unclealex.devicesynchroniser.repository.SummaryRepository
import uk.co.unclealex.devicesynchroniser.repository.SynchronisingStatusRepository
import javax.inject.Inject

@AndroidEntryPoint
class PreferencesFragment : PreferenceFragmentCompat() {

    @Inject
    lateinit var deviceRepository: DeviceRepository

    @Inject
    lateinit var summaryRepository: SummaryRepository

    @Inject
    lateinit var synchronisingStatusRepository: SynchronisingStatusRepository

    private fun sharedPreferences(): SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(this.requireContext())

    private fun valueOf(key: String): String {
        val value = sharedPreferences().getString(key, null)
        return if (value.isNullOrBlank()) "Not set" else value
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
        mapOf(
            Pair(KEY_DEVICE_IDENTIFIER, R.string.summary_device_identifier),
            Pair(KEY_SERVER_URL, R.string.summary_server_url),
        ).forEach { (key, summaryId) ->
            findPreference<EditTextPreference>(key)?.summaryProvider =
                Preference.SummaryProvider<EditTextPreference> {
                    "${getString(summaryId)}\n${valueOf(key)}"
                }
        }
        findPreference<Preference>(KEY_CLEAR_DATABASES)?.onPreferenceClickListener =
            Preference.OnPreferenceClickListener {
                Completable.concatArray(
                    summaryRepository.clearCache(),
                    deviceRepository.clearCache(),
                    synchronisingStatusRepository.clearCache(),
                ).blockingAwait()
                true
            }
    }
}
