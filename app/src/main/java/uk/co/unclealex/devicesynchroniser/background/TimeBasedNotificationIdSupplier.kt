package uk.co.unclealex.devicesynchroniser.background

import timber.log.Timber
import java.time.Clock
import java.time.LocalDateTime

class TimeBasedNotificationIdSupplier(private val clock: Clock) : NotificationIdSupplier {

    private var currentId: Int? = null

    override fun next(): Int {
        val now = LocalDateTime.now(clock)
        val fromYear: Int = now.year - 2040
        val fromMonth: Int = fromYear * 12 + now.monthValue - 1
        val fromDay: Int = fromMonth * 35 + now.dayOfMonth - 1
        val fromHour: Int = fromDay * 24 + now.hour
        val fromMinute: Int = fromHour * 60 + now.minute
        Timber.i("Next notification id is [$fromMinute]")
        currentId = fromMinute
        return currentId!!
    }

    override fun current(): Int {
        if (currentId == null) {
            return next()
        } else {
            return currentId!!
        }
    }
}
