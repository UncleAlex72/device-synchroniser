package uk.co.unclealex.devicesynchroniser.util

import androidx.work.Data
import uk.co.unclealex.devicesynchroniser.db.Tokenisable
import java.time.Instant

private fun <T> maybePut(
    value: T?,
    dataBuilder: Data.Builder,
    builderPutter: (Data.Builder, T) -> Data.Builder,
): Data.Builder {
    return if (value == null) {
        dataBuilder
    } else {
        builderPutter(dataBuilder, value)
    }
}

fun Data.Builder.putMaybeInt(key: String, value: Int?): Data.Builder {
    return maybePut(value, this) { builder, v -> builder.putInt(key, v) }
}

fun Data.Builder.putMaybeBoolean(key: String, value: Boolean?): Data.Builder {
    return maybePut(value, this) { builder, v -> builder.putBoolean(key, v) }
}

fun Data.Builder.putMaybeString(key: String, value: String?): Data.Builder {
    return maybePut(value, this) { builder, v -> builder.putString(key, v) }
}

fun Data.Builder.putSub(key: String, data: Data?): Data.Builder {
    return maybePut(data, this) { builder, _data ->
        val subData = _data.keyValueMap.mapKeys { kv -> "$key:${kv.key}" }
        builder.putAll(subData)
    }
}

fun Data.getSub(key: String): Data? {
    val prefix = "$key:"
    val prefixLength = prefix.length
    val subData = this.keyValueMap.filterKeys { it.startsWith(prefix) }
        .mapKeys { it.key.substring(prefixLength) }
    return if (subData.isEmpty()) {
        null
    } else {
        Data.Builder().putAll(subData).build()
    }
}

fun Data.requireSub(key: String): Data {
    return getSub(key)
        ?: throw IllegalArgumentException("Cannot find a required sub-data value for work data key $key")
}

fun <TOKEN : Tokenisable> Data.Builder.putToken(key: String, token: TOKEN): Data.Builder {
    return putString(key, token.token)
}

fun <TOKEN : Tokenisable> Data.getToken(key: String, values: Array<TOKEN>): TOKEN? {
    return getString(key)?.let {
        val value = it
        values.find { token -> value == token.token }
            ?: throw IllegalArgumentException("$value is not a valid token")
    }
}

fun <TOKEN : Tokenisable> Data.requireToken(key: String, values: Array<TOKEN>): TOKEN {
    return getToken(key, values)
        ?: throw IllegalArgumentException("Cannot find a required token for work data key $key")
}

fun Data.Builder.putInstant(key: String, instant: Instant?): Data.Builder {
    return (instant?.let { this.putLong(key, it.toEpochMilli()) }) ?: this
}

fun Data.int(key: String): Int? {
    val result = this.getInt(key, -1)
    return if (result < 0) null else result
}

fun Data.long(key: String): Long? {
    val result = this.getLong(key, -1)
    return if (result < 0) null else result
}

fun Data.string(key: String): String? {
    return this.getString(key)
}

fun Data.bool(key: String): Boolean {
    return this.getBoolean(key, false)
}

fun Data.requireInt(key: String): Int {
    return int(key)
        ?: throw IllegalArgumentException("Cannot find a required integer value for work data key $key")
}

fun Data.requireString(key: String): String {
    return string(key)
        ?: throw IllegalArgumentException("Cannot find a required string value for work data key $key")
}

fun Data.instant(key: String): Instant? {
    return long(key)?.let { Instant.ofEpochMilli(it) }
}

fun Data.requireInstant(key: String): Instant {
    return instant(key)
        ?: throw IllegalArgumentException("Cannot find a required instant value for work data key $key")
}

fun Throwable.toWorkData(): Data {
    return Data.Builder().putString("exceptionClassName", this.javaClass.name)
        .putMaybeString("message", this.message).build()
}

fun Data.toException(): Pair<String, String?>? {
    val exceptionClassName = getString("exceptionClassName")
    return exceptionClassName?.let {
        Pair(exceptionClassName, getString("message"))
    }
}
