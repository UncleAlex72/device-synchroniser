/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.co.unclealex.devicesynchroniser.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Flowable
import uk.co.unclealex.devicesynchroniser.vo.SynchronisingStatus

/**
 * Interface for database access for Summary related operations.
 */
@Dao
interface SynchronisingStatusDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateStatusSync(synchronisingStatus: SynchronisingStatus)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateStatus(synchronisingStatus: SynchronisingStatus): Completable

    @Delete
    fun deleteStatusSync(synchronisingStatus: SynchronisingStatus)

    @Query("SELECT * FROM synchronisingstatus where id = :id")
    fun findByIdSync(id: Int): SynchronisingStatus?

    @Query("SELECT * FROM synchronisingstatus where id = :id")
    fun findByIdLiveData(id: Int): LiveData<SynchronisingStatus?>

    @Query("SELECT * FROM synchronisingstatus where id = :id")
    fun findById(id: Int): LiveData<SynchronisingStatus?>

    @Query("SELECT * FROM synchronisingstatus")
    fun findAll(): Flowable<List<SynchronisingStatus>>

    @Query("DELETE FROM synchronisingstatus")
    fun deleteAll(): Completable

    @Query("DELETE FROM synchronisingstatus")
    fun deleteAllSync()
}
