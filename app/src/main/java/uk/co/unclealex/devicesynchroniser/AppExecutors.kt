/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.co.unclealex.devicesynchroniser

import android.os.Handler
import android.os.Looper
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Global executor pools for the whole application.
 *
 * Grouping tasks like this avoids the effects of task starvation (e.g. disk reads don't wait behind
 * webservice requests).
 */
@Singleton
open class AppExecutors(
    private val diskIoExecutor: Executor,
    private val networkIoExecutor: Executor,
    private val mainThreadExecutor: Executor,
) {

    private val diskIoScheduler: Scheduler = Schedulers.from(diskIoExecutor)
    private val networkIoScheduler: Scheduler = Schedulers.from(networkIoExecutor)
    private val mainThreadScheduler: Scheduler = Schedulers.from(mainThreadExecutor)

    @Inject
    constructor() : this(
        Executors.newSingleThreadExecutor(),
        Executors.newFixedThreadPool(3),
        MainThreadExecutor(),
    )

    fun diskIoExecutor(): Executor {
        return diskIoExecutor
    }

    fun diskIoScheduler(): Scheduler {
        return diskIoScheduler
    }

    fun networkIoExecutor(): Executor {
        return networkIoExecutor
    }

    fun networkIoScheduler(): Scheduler {
        return networkIoScheduler
    }

    fun mainThreadExecutor(): Executor {
        return mainThreadExecutor
    }

    fun mainThreadScheduler(): Scheduler {
        return mainThreadScheduler
    }

    private class MainThreadExecutor : Executor {
        private val mainThreadHandler = Handler(Looper.getMainLooper())
        override fun execute(command: Runnable) {
            mainThreadHandler.post(command)
        }
    }
}
