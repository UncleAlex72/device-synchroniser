/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.co.unclealex.devicesynchroniser.vo

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import androidx.work.Data
import uk.co.unclealex.devicesynchroniser.util.bool
import uk.co.unclealex.devicesynchroniser.util.getSub
import uk.co.unclealex.devicesynchroniser.util.int
import uk.co.unclealex.devicesynchroniser.util.putMaybeInt
import uk.co.unclealex.devicesynchroniser.util.putSub
import uk.co.unclealex.devicesynchroniser.util.requireInt

@Entity
data class SynchronisingStatus(
    @PrimaryKey(autoGenerate = false)
    @field:ColumnInfo(name = "id")
    val id: Int,
    @field:Embedded(prefix = "delta_")
    val delta: Delta?,
    @field:Embedded(prefix = "summary_")
    val summary: Summary?,
    @field:ColumnInfo(name = "smry_progress")
    val summaryProgress: Int?,
    @field:ColumnInfo(name = "last")
    val last: Boolean,
    @field:ColumnInfo(name = "progress")
    val progress: Int,
    @field:ColumnInfo(name = "total")
    val total: Int,
) {

    @Ignore
    val flattened = summary?.flattened

    fun toWorkData(): Data {
        return Data.Builder().putInt(ID, id).putMaybeInt(SUMMARY_PROGRESS, summaryProgress)
            .putSub(SUMMARY, summary?.toWorkData()).putBoolean(LAST, last)
            .putInt(PROGRESS, progress).putInt(TOTAL, total).putSub(DELTA, delta?.toWorkData())
            .build()
    }

    companion object {
        const val STATIC_ID = 0

        val START =
            SynchronisingStatus(
                STATIC_ID,
                delta = null,
                summary = null,
                summaryProgress = null,
                last = false,
                progress = 0,
                total = 0,
            )

        private const val ID = "id"
        private const val DELTA = "delta_ordering"
        private const val SUMMARY = "summary"
        private const val SUMMARY_PROGRESS = "summary_progress"
        private const val PROGRESS = "progress"
        private const val LAST = "last"
        private const val TOTAL = "total"

        fun fromWorkData(data: Data): SynchronisingStatus {
            val id = data.requireInt(ID)
            val delta = data.getSub(DELTA)?.let { Delta.fromWorkData(it) }
            val summary = data.getSub(SUMMARY)?.let { Summary.fromWorkData(it) }
            val summaryProgress = data.int(SUMMARY_PROGRESS)
            val last = data.bool(LAST)
            val progress = data.requireInt(PROGRESS)
            val total = data.requireInt(TOTAL)
            return SynchronisingStatus(
                id = id,
                delta = delta,
                summary = summary,
                summaryProgress = summaryProgress,
                last = last,
                progress = progress,
                total = total,
            )
        }
    }
}
