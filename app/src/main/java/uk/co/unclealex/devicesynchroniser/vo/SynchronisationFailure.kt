package uk.co.unclealex.devicesynchroniser.vo

data class SynchronisationFailure(val exceptionClass: String, val message: String?)
