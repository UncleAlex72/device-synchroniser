/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.co.unclealex.devicesynchroniser.repository

import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.functions.Consumer
import uk.co.unclealex.devicesynchroniser.AppExecutors
import uk.co.unclealex.devicesynchroniser.api.WebService
import uk.co.unclealex.devicesynchroniser.api.WebServiceFactory
import uk.co.unclealex.devicesynchroniser.db.DeviceDao
import uk.co.unclealex.devicesynchroniser.settings.Settings
import uk.co.unclealex.devicesynchroniser.vo.Device
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Repository that handles Summary instances.
 *
 * unfortunate naming :/ .
 * Summary - value object name
 * Repository - type of this class.
 */
@Singleton
class DeviceRepository @Inject constructor(
    private val deviceDao: DeviceDao,
    appExecutors: AppExecutors,
    webServiceFactory: WebServiceFactory,
    settings: Settings,
    onNetworkError: Consumer<String>,
) :
    ResourceRepository<Device>(appExecutors, webServiceFactory, settings, onNetworkError) {

    override fun clearDb(): Completable {
        return deviceDao.deleteAll()
    }

    override fun saveData(item: Device): Completable {
        return deviceDao.insertDevice(item)
    }

    override fun loadFromDb() = deviceDao.find()

    override fun fetchFromNetwork(
        webService: WebService,
        identifier: String,
    ): Single<Device> = webService.getDeviceInfo(identifier)
}
