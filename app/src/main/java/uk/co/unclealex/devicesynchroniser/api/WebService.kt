/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.co.unclealex.devicesynchroniser.api

import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Path
import uk.co.unclealex.devicesynchroniser.vo.Delta
import uk.co.unclealex.devicesynchroniser.vo.Device
import uk.co.unclealex.devicesynchroniser.vo.Summary

/**
 * REST API access points
 */
interface WebService {

    @GET("/deltas/full/{identifier}")
    fun getDeltas(@Path("identifier") identifier: String): Single<List<Delta>>

    @GET("/deltas/summaries/{identifier}")
    fun getSummaries(@Path("identifier") identifier: String): Single<List<Summary>>

    @GET("/devices/{identifier}/info")
    fun getDeviceInfo(@Path("identifier") identifier: String): Single<Device>

    @PUT("/devices/{identifier}/lastSynchronised")
    fun updateLastSynchronised(@Path("identifier") identifier: String): Call<Device>

    @PUT("/devices/{identifier}/offset")
    fun updateOffset(@Path("identifier") identifier: String, @Body offset: Int): Call<Device>
}
