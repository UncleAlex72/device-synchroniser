/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.co.unclealex.devicesynchroniser.repository

import io.reactivex.Flowable
import uk.co.unclealex.devicesynchroniser.db.SynchronisingStatusDao
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CanSynchroniseRepository @Inject constructor(
    private val summaryRepository: SummaryRepository,
    private val synchronisingStatusDao: SynchronisingStatusDao,
) {

    fun canSynchronise(): Flowable<Boolean> {
        data class FabState(val itemCount: Int, val synchronising: Boolean) {
            fun enabled(): Boolean {
                return itemCount != 0 && !synchronising
            }
        }

        abstract class FabStateMutator {
            abstract fun mutate(currentState: FabState): FabState
        }

        val summariesCount: Flowable<FabStateMutator> =
            summaryRepository.loadResource().map { resource ->
                object : FabStateMutator() {
                    override fun mutate(currentState: FabState): FabState {
                        return currentState.copy(itemCount = resource.data?.size ?: 0)
                    }
                }
            }

        val synchronising: Flowable<FabStateMutator> =
            synchronisingStatusDao.findAll().map { statuses ->
                object : FabStateMutator() {
                    override fun mutate(currentState: FabState): FabState {
                        return currentState.copy(synchronising = statuses.isNotEmpty())
                    }
                }
            }
        val fabState: Flowable<FabState> =
            summariesCount.mergeWith(synchronising)
                .scanWith({ FabState(0, false) }) { state, mutator ->
                    mutator.mutate(state)
                }
        return fabState.map { it.enabled() }
    }
}
