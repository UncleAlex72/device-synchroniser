package uk.co.unclealex.devicesynchroniser.background

import android.content.Context
import androidx.hilt.work.HiltWorker
import androidx.work.Worker
import androidx.work.WorkerParameters
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import timber.log.Timber
import uk.co.unclealex.devicesynchroniser.synchronise.SynchronisingService
import uk.co.unclealex.devicesynchroniser.vo.Delta
import uk.co.unclealex.devicesynchroniser.vo.SynchronisingStatus
import java.util.Locale

@HiltWorker
class SynchronisingWorker @AssistedInject constructor(
    private val synchronisingService: SynchronisingService,
    private val notificationService: NotificationService,
    @Assisted private val appContext: Context,
    @Assisted private val workerParams: WorkerParameters,
) : Worker(appContext, workerParams) {

    data class NotificationText(val album: String, val title: String)

    override fun doWork(): Result {
        val synchronisingStatus = SynchronisingStatus.fromWorkData(inputData)
        val delta: Delta? = synchronisingStatus.delta
        val path = delta?.path ?: ""
        return try {
            // Do download
            val track = delta?.track
            val notificationText = if (track == null) {
                NotificationText(path, "")
            } else {
                NotificationText(
                    track.album,
                    String.format(Locale.UK, "%02d %s", track.trackNumber, track.title),
                )
            }

            notificationService.updateNotification(
                notificationText.album,
                notificationText.title,
                synchronisingStatus.progress,
                synchronisingStatus.total,
            )
            synchronisingService.synchronise(synchronisingStatus)
            Result.success()
        } catch (e: Exception) {
            Timber.e(e, "Cannot download $path")
            Result.retry()
        }
    }
}
