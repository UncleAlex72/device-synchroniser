package uk.co.unclealex.devicesynchroniser.repository

import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.functions.Consumer
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import uk.co.unclealex.devicesynchroniser.AppExecutors
import uk.co.unclealex.devicesynchroniser.api.WebService
import uk.co.unclealex.devicesynchroniser.api.WebServiceFactory
import uk.co.unclealex.devicesynchroniser.settings.Settings
import uk.co.unclealex.devicesynchroniser.vo.Resource

abstract class ResourceRepository<T>(
    private val appExecutors: AppExecutors,
    private val webServiceFactory: WebServiceFactory,
    private val settings: Settings,
    private val onNetworkError: Consumer<String>,
) {

    private val statusSubject = PublishSubject.create<Resource<T>>()
    private val statusFlowable = statusSubject.toFlowable(BackpressureStrategy.BUFFER)

    fun loadResource(): Flowable<Resource<T>> {
        val dbFlowable = loadFromDb().map { t -> Resource.success(t) }
        return Flowable.merge(dbFlowable, statusFlowable)
    }

    fun refresh() {
        when (val canRefresh = canRefresh()) {
            is Refreshable.CanRefresh -> {
                statusSubject.onNext(Resource.loading())
                val onSuccess = Consumer<T> { t ->
                    saveData(t).observeOn(appExecutors.diskIoScheduler())
                        .toSingleDefault(Resource.refreshed<T>()).flattenAsFlowable {
                            arrayListOf(it) + emitAfterRefreshed(t).map { ear ->
                                Resource.success(
                                    ear,
                                )
                            }
                        }.subscribe {
                            statusSubject.onNext(it)
                        }
                }
                val onError = Consumer<Throwable> { ex ->
                    Timber.e(ex)
                    onNetworkError.accept("A network error occurred.")
                    loadFromDb().firstElement().observeOn(appExecutors.diskIoScheduler())
                        .flattenAsFlowable { t ->
                            arrayListOf(
                                Resource.refreshed(),
                                Resource.success(t),
                            )
                        }.subscribe { r ->
                            statusSubject.onNext(r)
                        }
                }
                fetchFromNetwork(canRefresh.webService, canRefresh.identifier).subscribe(
                    onSuccess,
                    onError,
                )
            }

            is Refreshable.CannotRefresh -> {
                statusSubject.onNext(Resource.refreshed())
            }
        }
    }

    private fun canRefresh(): Refreshable {
        val identifier = settings.getDeviceIdentifier()
        val webService = webServiceFactory.build(settings)
        return if (identifier != null && webService != null) {
            Refreshable.CanRefresh(webService, identifier)
        } else {
            Refreshable.CannotRefresh
        }
    }

    sealed class Refreshable {
        data class CanRefresh(val webService: WebService, val identifier: String) : Refreshable()
        object CannotRefresh : Refreshable()
    }

    fun clearCache(): Completable {
        return clearDb().subscribeOn(appExecutors.diskIoScheduler())
    }

    open fun emitAfterRefreshed(item: T): List<T> = emptyList()

    abstract fun fetchFromNetwork(webService: WebService, identifier: String): Single<T>

    abstract fun loadFromDb(): Flowable<T>

    abstract fun saveData(item: T): Completable

    abstract fun clearDb(): Completable
}
