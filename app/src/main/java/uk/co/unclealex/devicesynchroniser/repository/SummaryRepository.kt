/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.co.unclealex.devicesynchroniser.repository

import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.functions.Consumer
import uk.co.unclealex.devicesynchroniser.AppExecutors
import uk.co.unclealex.devicesynchroniser.api.WebService
import uk.co.unclealex.devicesynchroniser.api.WebServiceFactory
import uk.co.unclealex.devicesynchroniser.db.SummaryDao
import uk.co.unclealex.devicesynchroniser.settings.Settings
import uk.co.unclealex.devicesynchroniser.vo.Summary
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Repository that handles Summary instances.
 *
 * unfortunate naming :/ .
 * Summary - value object name
 * Repository - type of this class.
 */
@Singleton
class SummaryRepository @Inject constructor(
    private val summaryDao: SummaryDao,
    appExecutors: AppExecutors,
    webServiceFactory: WebServiceFactory,
    settings: Settings,
    onNetworkError: Consumer<String>,
) :
    ResourceRepository<List<Summary>>(appExecutors, webServiceFactory, settings, onNetworkError) {

    /**
     * Nasty hack to get around deleting a whole table not emitting a change
     */
    override fun emitAfterRefreshed(item: List<Summary>): List<List<Summary>> {
        return if (item.isEmpty()) arrayListOf(item) else emptyList()
    }

    override fun clearDb(): Completable {
        return summaryDao.deleteAll()
    }

    override fun saveData(item: List<Summary>): Completable {
        return summaryDao.deleteAll().andThen(summaryDao.insertSummaries(item))
    }

    override fun loadFromDb() = summaryDao.findAll()

    override fun fetchFromNetwork(
        webService: WebService,
        identifier: String,
    ): Single<List<Summary>> = webService.getSummaries(identifier)
}
