package uk.co.unclealex.devicesynchroniser.background

import android.content.Context
import androidx.hilt.work.HiltWorker
import androidx.work.Worker
import androidx.work.WorkerParameters
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import uk.co.unclealex.devicesynchroniser.synchronise.SynchronisingService

@HiltWorker
class StartSynchronisingWorker @AssistedInject constructor(
    private val synchronisingService: SynchronisingService,
    private val notificationService: NotificationService,
    @Assisted private val appContext: Context,
    @Assisted private val workerParams: WorkerParameters,
) : Worker(appContext, workerParams) {

    override fun doWork(): Result {
        return try {
            notificationService.createNotification()
            synchronisingService.startSynchronising()
            Result.success()
        } catch (e: Exception) {
            Result.failure()
        }
    }
}
