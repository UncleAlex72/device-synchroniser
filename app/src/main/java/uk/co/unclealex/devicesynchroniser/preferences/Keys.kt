package uk.co.unclealex.devicesynchroniser.preferences

object Keys {
    const val KEY_DEVICE_IDENTIFIER = "device_identifier"
    const val KEY_SERVER_URL = "server_url"
    const val KEY_CLEAR_DATABASES = "clear_databases"
}
