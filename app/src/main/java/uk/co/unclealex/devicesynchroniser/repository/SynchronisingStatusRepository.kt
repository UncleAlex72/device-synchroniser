package uk.co.unclealex.devicesynchroniser.repository

import io.reactivex.Completable
import io.reactivex.Flowable
import uk.co.unclealex.devicesynchroniser.AppExecutors
import uk.co.unclealex.devicesynchroniser.db.SynchronisingStatusDao
import uk.co.unclealex.devicesynchroniser.vo.SynchronisingStatus
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SynchronisingStatusRepository @Inject constructor(
    private val appExecutors: AppExecutors,
    private val synchronisingStatusDao: SynchronisingStatusDao,
) {

    fun currentSynchronisingStatus(): Flowable<List<SynchronisingStatus>> {
        return synchronisingStatusDao.findAll()
    }

    fun clearCache(): Completable {
        return synchronisingStatusDao.deleteAll().subscribeOn(appExecutors.diskIoScheduler())
    }
}
