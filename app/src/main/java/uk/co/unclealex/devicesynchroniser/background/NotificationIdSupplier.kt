package uk.co.unclealex.devicesynchroniser.background

interface NotificationIdSupplier {

    fun next(): Int

    fun current(): Int
}
