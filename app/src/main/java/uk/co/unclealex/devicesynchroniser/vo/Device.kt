/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.co.unclealex.devicesynchroniser.vo

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import java.time.Instant

@Entity(primaryKeys = ["identifier"])
data class Device(
    @field:SerializedName("identifier")
    @field:ColumnInfo(name = "identifier")
    val identifier: String,
    @field:SerializedName("displayName")
    @field:ColumnInfo(name = "displayName")
    val displayName: String,
    @field:SerializedName("lastSynchronised")
    @field:ColumnInfo(name = "lastSynchronised")
    val lastSynchronised: Instant?,
)
