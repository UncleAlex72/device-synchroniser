package uk.co.unclealex.devicesynchroniser.api

import uk.co.unclealex.devicesynchroniser.settings.Settings

interface WebServiceFactory {

    fun build(settings: Settings): WebService?
}
