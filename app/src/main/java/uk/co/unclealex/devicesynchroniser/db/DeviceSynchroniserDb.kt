/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.co.unclealex.devicesynchroniser.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import uk.co.unclealex.devicesynchroniser.vo.Device
import uk.co.unclealex.devicesynchroniser.vo.Summary
import uk.co.unclealex.devicesynchroniser.vo.SynchronisingStatus

/**
 * Main database description.
 */
@Database(
    entities = [
        Device::class,
        Summary::class,
        SynchronisingStatus::class,
    ],
    version = 17,
)
@TypeConverters(DeviceSynchroniserTypeConverters::class)
abstract class DeviceSynchroniserDb : RoomDatabase() {

    abstract fun summaryDao(): SummaryDao

    abstract fun deviceDao(): DeviceDao

    abstract fun synchronisingStatusDao(): SynchronisingStatusDao
}
