/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.co.unclealex.devicesynchroniser.di

import android.app.Application
import android.content.Context
import android.widget.Toast
import androidx.preference.PreferenceManager
import androidx.room.Room
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.reactivex.functions.Consumer
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import uk.co.unclealex.devicesynchroniser.AppExecutors
import uk.co.unclealex.devicesynchroniser.api.DeviceSynchroniserTypeAdapters
import uk.co.unclealex.devicesynchroniser.api.WebService
import uk.co.unclealex.devicesynchroniser.api.WebServiceFactory
import uk.co.unclealex.devicesynchroniser.background.NotificationIdSupplier
import uk.co.unclealex.devicesynchroniser.background.NotificationService
import uk.co.unclealex.devicesynchroniser.background.TimeBasedNotificationIdSupplier
import uk.co.unclealex.devicesynchroniser.db.DeviceDao
import uk.co.unclealex.devicesynchroniser.db.DeviceSynchroniserDb
import uk.co.unclealex.devicesynchroniser.db.SummaryDao
import uk.co.unclealex.devicesynchroniser.db.SynchronisingStatusDao
import uk.co.unclealex.devicesynchroniser.preferences.Keys
import uk.co.unclealex.devicesynchroniser.repository.DeviceRepository
import uk.co.unclealex.devicesynchroniser.repository.SummaryRepository
import uk.co.unclealex.devicesynchroniser.settings.Settings
import uk.co.unclealex.devicesynchroniser.synchronise.SynchronisingService
import uk.co.unclealex.devicesynchroniser.vo.Delta
import uk.co.unclealex.devicesynchroniser.vo.Summary
import java.text.DateFormat
import java.time.Clock
import java.time.Instant
import java.util.Locale
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Singleton
    @Provides
    fun context(application: Application): Context {
        return application.applicationContext
    }

    @Singleton
    @Provides
    fun provideSettings(context: Context): Settings {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        fun preference(key: String): String? = sharedPreferences.getString(key, null)
        return object : Settings {
            override fun getRootSeverUrl(): String? {
                return preference(Keys.KEY_SERVER_URL)
            }

            override fun getDeviceIdentifier(): String? {
                return preference(Keys.KEY_DEVICE_IDENTIFIER)?.lowercase(Locale.getDefault())
            }
        }
    }

    @Singleton
    @Provides
    fun provideClock(): Clock = Clock.systemDefaultZone()

    @Singleton
    @Provides
    fun provideGson(): Gson {
        return GsonBuilder()
            .registerTypeAdapter(
                Delta.Type::class.java,
                DeviceSynchroniserTypeAdapters.deltaTypeAdapter,
            )
            .registerTypeAdapter(
                Summary.Action::class.java,
                DeviceSynchroniserTypeAdapters.summaryActionAdapter,
            )
            .registerTypeAdapter(
                Instant::class.java,
                DeviceSynchroniserTypeAdapters.instantTypeAdapter,
            )
            .setDateFormat(DateFormat.LONG)
            .create()
    }

    @Singleton
    @Provides
    fun provideNotificationIdSupplier(clock: Clock): NotificationIdSupplier {
        return TimeBasedNotificationIdSupplier(clock)
    }

    @Singleton
    @Provides
    fun provideNotificationService(notificationIdSupplier: NotificationIdSupplier, context: Context): NotificationService {
        return NotificationService(context, notificationIdSupplier)
    }

    @Singleton
    @Provides
    fun provideSynchronisingService(
        summaryDao: SummaryDao,
        deviceDao: DeviceDao,
        synchronisingStatusDao: SynchronisingStatusDao,
        webServiceFactory: WebServiceFactory,
        context: Context,
        appExecutors: AppExecutors,
        summaryRepository: SummaryRepository,
        deviceRepository: DeviceRepository,
        settings: Settings,
    ): SynchronisingService {
        return SynchronisingService(
            summaryDao = summaryDao,
            deviceDao = deviceDao,
            synchronisingStatusDao = synchronisingStatusDao,
            webServiceFactory = webServiceFactory,
            summaryRepository = summaryRepository,
            deviceRepository = deviceRepository,
            appExecutors = appExecutors,
            context = context,
            settings = settings,
        )
    }

    @Singleton
    @Provides
    fun provideWebService(gson: Gson, appExecutors: AppExecutors): WebServiceFactory {
        return object : WebServiceFactory {
            override fun build(settings: Settings): WebService? {
                val rootSeverUrl = settings.getRootSeverUrl()
                return try {
                    if (rootSeverUrl != null) {
                        val rxJava2CallAdapterFactory =
                            RxJava2CallAdapterFactory.createWithScheduler(appExecutors.networkIoScheduler())
                        Retrofit.Builder()
                            .baseUrl(rootSeverUrl)
                            .addConverterFactory(GsonConverterFactory.create(gson))
                            .addCallAdapterFactory(rxJava2CallAdapterFactory)
                            .build()
                            .create(WebService::class.java)
                    } else {
                        null
                    }
                } catch (e: Exception) {
                    null
                }
            }
        }
    }

    @Singleton
    @Provides
    fun provideDb(app: Application): DeviceSynchroniserDb {
        return Room
            .databaseBuilder(app, DeviceSynchroniserDb::class.java, "device-synchroniser.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideSummaryDao(db: DeviceSynchroniserDb): SummaryDao {
        return db.summaryDao()
    }

    @Singleton
    @Provides
    fun provideSynchronisingStatusDao(db: DeviceSynchroniserDb): SynchronisingStatusDao {
        return db.synchronisingStatusDao()
    }

    @Singleton
    @Provides
    fun provideDeviceDao(db: DeviceSynchroniserDb): DeviceDao {
        return db.deviceDao()
    }

    @Singleton
    @Provides
    fun providesOnNetworkError(context: Context, appExecutors: AppExecutors): Consumer<String> {
        return Consumer { message ->
            message?.let {
                appExecutors.mainThreadExecutor().execute {
                    val toast = Toast.makeText(context, message, Toast.LENGTH_SHORT)
                    toast.show()
                }
            }
        }
    }
}
