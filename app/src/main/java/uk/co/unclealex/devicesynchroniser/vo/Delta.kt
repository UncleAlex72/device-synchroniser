/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.co.unclealex.devicesynchroniser.vo

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.work.Data
import com.google.gson.annotations.SerializedName
import uk.co.unclealex.devicesynchroniser.db.Tokenisable
import uk.co.unclealex.devicesynchroniser.util.bool
import uk.co.unclealex.devicesynchroniser.util.getSub
import uk.co.unclealex.devicesynchroniser.util.instant
import uk.co.unclealex.devicesynchroniser.util.putInstant
import uk.co.unclealex.devicesynchroniser.util.putMaybeBoolean
import uk.co.unclealex.devicesynchroniser.util.putSub
import uk.co.unclealex.devicesynchroniser.util.putToken
import uk.co.unclealex.devicesynchroniser.util.requireInt
import uk.co.unclealex.devicesynchroniser.util.requireString
import uk.co.unclealex.devicesynchroniser.util.requireToken
import uk.co.unclealex.devicesynchroniser.util.string
import java.time.Instant

data class Delta(
    @field:SerializedName("type")
    @field:ColumnInfo(name = "type")
    val type: Type,
    @field:SerializedName("path")
    @field:ColumnInfo(name = "path")
    val path: String,
    @field:SerializedName("ordering")
    @field:ColumnInfo(name = "ordering")
    val ordering: Int,
    @field:SerializedName("timestamp")
    @field:ColumnInfo(name = "timestamp")
    val timestamp: Instant?,
    @field:SerializedName("url")
    @field:ColumnInfo(name = "url")
    val url: String?,
    @field:SerializedName("mimeType")
    @field:ColumnInfo(name = "mime_type")
    val mimeType: String?,
    @field:SerializedName("track")
    @field:Embedded(prefix = "track_")
    val track: Track?,
    @field:SerializedName("summaryPath")
    @field:ColumnInfo(name = "summary_path")
    val summaryPath: String?,
    @field:SerializedName("last")
    @field:ColumnInfo(name = "last")
    val last: Boolean?,

) {

    data class Track(
        @field:SerializedName("trackNumber")
        @field:ColumnInfo(name = "trackNumber")
        val trackNumber: Int,
        @field:SerializedName("totalTracks")
        @field:ColumnInfo(name = "totalTracks")
        val totalTracks: Int,
        @field:SerializedName("title")
        @field:ColumnInfo(name = "title")
        val title: String,
        @field:SerializedName("album")
        @field:ColumnInfo(name = "album")
        val album: String,
        @field:SerializedName("artist")
        @field:ColumnInfo(name = "artist")
        val artist: String,
    ) {

        fun toWorkData(): Data {
            return Data.Builder().putInt("trackNumber", trackNumber)
                .putInt("totalTracks", totalTracks).putString("title", title)
                .putString("album", album).putString("artist", artist).build()
        }

        companion object {
            fun fromWorkData(data: Data): Track {
                val trackNumber = data.requireInt("trackNumber")
                val totalTracks = data.requireInt("totalTracks")
                val title = data.requireString("title")
                val album = data.requireString("album")
                val artist = data.requireString("artist")
                return Track(
                    trackNumber = trackNumber,
                    totalTracks = totalTracks,
                    title = title,
                    album = album,
                    artist = artist,
                )
            }
        }
    }

    enum class Type(override val token: String) : Tokenisable {
        CREATE_FILE("createFile"),
        REMOVE_FILE("removeFile"),
    }

    fun toWorkData(): Data {
        return Data.Builder().putToken("type", type).putString("path", path)
            .putInt("ordering", ordering).putInstant("timestamp", timestamp).putString("url", url)
            .putSub("track", track?.let { it.toWorkData() }).putString("mimeType", mimeType)
            .putString("summary", summaryPath).putMaybeBoolean("last", last).build()
    }

    companion object {
        fun fromWorkData(data: Data): Delta {
            val type = data.requireToken("type", Type.values())
            val path = data.requireString("path")
            val ordering = data.requireInt("ordering")
            val timestamp = data.instant("timestamp")
            val url = data.string("url")
            val mimeType = data.string("mimeType")
            val track = data.getSub("track")?.let { Track.fromWorkData(it) }
            val summaryPath = data.requireString("summary")
            val last = data.bool("last")
            return Delta(
                type = type,
                path = path,
                ordering = ordering,
                timestamp = timestamp,
                url = url,
                mimeType = mimeType,
                track = track,
                summaryPath = summaryPath,
                last = last,
            )
        }
    }
}
