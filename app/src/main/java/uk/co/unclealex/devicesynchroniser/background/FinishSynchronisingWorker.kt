package uk.co.unclealex.devicesynchroniser.background

import android.content.Context
import androidx.hilt.work.HiltWorker
import androidx.work.Worker
import androidx.work.WorkerParameters
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import uk.co.unclealex.devicesynchroniser.synchronise.SynchronisingService

@HiltWorker
class FinishSynchronisingWorker @AssistedInject constructor(
    private val synchronisingService: SynchronisingService,
    private val notificationService: NotificationService,
    @Assisted private val appContext: Context,
    @Assisted private val workerParams: WorkerParameters,
) : Worker(appContext, workerParams) {

    override fun doWork(): Result {
        return try {
            synchronisingService.finishSynchronising(
                inputData.getBoolean(
                    FULLY_SYNCHRONISED,
                    false,
                ),
            )
            notificationService.finaliseNotification(inputData.getInt(TOTAL, 0))
            Result.success()
        } catch (e: Exception) {
            Result.failure()
        }
    }

    companion object {
        const val TOTAL = "total"
        const val FULLY_SYNCHRONISED = "fullySynchronised"
    }
}
