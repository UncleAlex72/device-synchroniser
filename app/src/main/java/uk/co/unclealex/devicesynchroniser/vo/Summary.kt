/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.co.unclealex.devicesynchroniser.vo

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.Index
import androidx.work.Data
import com.google.gson.annotations.SerializedName
import uk.co.unclealex.devicesynchroniser.db.Tokenisable
import uk.co.unclealex.devicesynchroniser.util.getSub
import uk.co.unclealex.devicesynchroniser.util.putMaybeString
import uk.co.unclealex.devicesynchroniser.util.putSub
import uk.co.unclealex.devicesynchroniser.util.putToken
import uk.co.unclealex.devicesynchroniser.util.requireInt
import uk.co.unclealex.devicesynchroniser.util.requireString
import uk.co.unclealex.devicesynchroniser.util.requireToken

@Entity(
    primaryKeys = ["path", "ordering", "action"],
    indices = [Index("ordering")],
)
data class Summary(
    @field:SerializedName("path")
    @field:ColumnInfo(name = "path")
    val path: String,
    @field:SerializedName("action")
    @field:ColumnInfo(name = "action")
    val action: Action,
    @field:SerializedName("ordering")
    @field:ColumnInfo(name = "ordering")
    val ordering: Int,
    @field:SerializedName("album")
    @field:Embedded(prefix = "album_")
    val album: Album?,
) {

    data class Album(
        @field:SerializedName("artist")
        @field:ColumnInfo(name = "artist")
        val artist: String,
        @field:SerializedName("title")
        @field:ColumnInfo(name = "title")
        val title: String,
        @field:SerializedName("tracks")
        @field:ColumnInfo(name = "tracks")
        val tracks: Int,
        @field:SerializedName("coverArtUrl")
        @field:ColumnInfo(name = "coverArtUrl")
        val coverArtUrl: String,

    ) {

        fun toWorkData(): Data {
            return Data.Builder().putString("artist", artist).putString("title", title)
                .putString("coverArtUrl", coverArtUrl)
                .putInt("tracks", tracks).build()
        }

        companion object {
            fun fromWorkData(data: Data): Album {
                val artist = data.requireString("artist")
                val title = data.requireString("title")
                val tracks = data.requireInt("tracks")
                val coverArtUrl = data.requireString("coverArtUrl")
                return Album(
                    artist = artist,
                    title = title,
                    tracks = tracks,
                    coverArtUrl = coverArtUrl,
                )
            }
        }
    }

    enum class Action(override val token: String) : Tokenisable {
        ADDED("added"), REMOVED("removed")
    }

    data class Flat(
        val path: String,
        val removing: Boolean,
        val ordering: Int,
        val artist: String?,
        val title: String?,
        val tracks: Int?,
        val coverArtUrl: String?,
    )

    @Ignore
    val flattened: Flat = Flat(
        path,
        action == Action.REMOVED,
        ordering,
        album?.artist,
        album?.title,
        album?.tracks,
        album?.coverArtUrl,
    )

    fun toWorkData(): Data {
        return Data.Builder().putString("path", path).putToken("action", action)
            .putInt("ordering", ordering).putSub("album", album?.toWorkData())
            .putMaybeString("coverArtUrl", album?.coverArtUrl).build()
    }

    companion object {
        fun fromWorkData(data: Data): Summary {
            val path = data.requireString("path")
            val action = data.requireToken("action", Action.values())
            val ordering = data.requireInt("ordering")
            val album = data.getSub("album")?.let { Album.fromWorkData(it) }
            return Summary(
                path = path,
                action = action,
                ordering = ordering,
                album = album,
            )
        }
    }
}
