package uk.co.unclealex.devicesynchroniser.background

import android.Manifest
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import uk.co.unclealex.devicesynchroniser.MainActivity
import uk.co.unclealex.devicesynchroniser.R
import javax.inject.Inject

class NotificationService @Inject constructor(val context: Context, val notificationIdSupplier: NotificationIdSupplier) {

    fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        val name = context.getString(R.string.channel_name)
        val descriptionText = context.getString(R.string.channel_description)
        val importance = NotificationManager.IMPORTANCE_LOW
        val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
            description = descriptionText
        }
        // Register the channel with the system
        val notificationManager: NotificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }

    fun createNotification() {
        val notificationId = notificationIdSupplier.next()
        showNotification(notificationId, null, null, 0, 1)
    }

    fun updateNotification(album: String?, track: String?, progress: Int, total: Int) {
        val notificationId = notificationIdSupplier.current()
        showNotification(notificationId, album, track, progress, total)
    }

    fun finaliseNotification(total: Int) {
        val notificationId = notificationIdSupplier.current()
        showNotification(notificationId, "Synchronising complete", "", total, total)
    }

    private fun showNotification(
        id: Int,
        title: String?,
        text: String?,
        progress: Int,
        total: Int,
    ) {
        val intent = Intent(context, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        }
        val pendingIntent: PendingIntent = PendingIntent.getActivity(
            context,
            0,
            intent,
            PendingIntent.FLAG_IMMUTABLE,
        )

        val builder = NotificationCompat.Builder(context, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_sync_black_24dp)
            .setContentTitle(title ?: "")
            .setContentText(text ?: "")
            .setContentIntent(pendingIntent)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setProgress(total, progress, false)

        with(NotificationManagerCompat.from(context)) {
            if (ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.POST_NOTIFICATIONS,
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                notify(id, builder.build())
            }
        }
    }

    companion object {
        private const val CHANNEL_ID: String = "MUSIC"
    }
}
