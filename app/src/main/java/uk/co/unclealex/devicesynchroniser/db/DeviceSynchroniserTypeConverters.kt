package uk.co.unclealex.devicesynchroniser.db

import androidx.room.TypeConverter
import uk.co.unclealex.devicesynchroniser.vo.Delta
import uk.co.unclealex.devicesynchroniser.vo.Summary
import java.time.Instant

class DeviceSynchroniserTypeConverters {

    @TypeConverter
    fun summaryActionToString(action: Summary.Action?): String? =
        action?.let { Tokenisable.asString(it) }

    @TypeConverter
    fun stringToSummaryAction(token: String?): Summary.Action? =
        token?.let { Tokenisable.asObject("summary action", Summary.Action.values(), it) }

    @TypeConverter
    fun deltaTypeToString(type: Delta.Type?): String? =
        type?.let { Tokenisable.asString(it) }

    @TypeConverter
    fun stringToDeltaType(token: String?): Delta.Type? =
        token?.let { Tokenisable.asObject("delta type", Delta.Type.values(), it) }

    @TypeConverter
    fun instantToLong(instant: Instant?): Long? = instant?.toEpochMilli()

    @TypeConverter
    fun longToInstant(millis: Long?): Instant? = millis?.let { Instant.ofEpochMilli(it) }
}
