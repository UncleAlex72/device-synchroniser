package uk.co.unclealex.devicesynchroniser.util

import io.reactivex.Maybe

object Maybes {

    fun <T> fromNullable(t: T?): Maybe<T> {
        return if (t == null) {
            Maybe.empty()
        } else {
            Maybe.just(t)
        }
    }
}
