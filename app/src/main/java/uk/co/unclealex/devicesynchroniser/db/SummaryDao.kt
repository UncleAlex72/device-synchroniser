/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.co.unclealex.devicesynchroniser.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe
import uk.co.unclealex.devicesynchroniser.vo.Summary

/**
 * Interface for database access for Summary related operations.
 */
@Dao
interface SummaryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSummaries(summaries: List<Summary>): Completable

    @Query("DELETE FROM summary")
    fun deleteAll(): Completable

    @Query("DELETE FROM summary")
    fun deleteAllSync()

    @Query("SELECT * FROM summary ORDER BY ordering")
    fun findAll(): Flowable<List<Summary>>

    @Query("SELECT * FROM summary ORDER BY ordering")
    fun maybeFindAll(): Maybe<List<Summary>>

    @Query("SELECT * from summary WHERE path = :path")
    fun findByPath(path: String): Maybe<Summary>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateSync(summary: Summary)

    @Delete
    fun deleteSync(summary: Summary)
}
