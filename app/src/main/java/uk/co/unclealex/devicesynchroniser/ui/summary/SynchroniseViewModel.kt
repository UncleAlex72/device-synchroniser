/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.co.unclealex.devicesynchroniser.ui.summary

import android.annotation.SuppressLint
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import uk.co.unclealex.devicesynchroniser.repository.SynchronisingStatusRepository
import uk.co.unclealex.devicesynchroniser.synchronise.SynchronisingService
import uk.co.unclealex.devicesynchroniser.vo.SynchronisationFailure
import javax.inject.Inject

@HiltViewModel
class SynchroniseViewModel @Inject constructor(
    private val synchronisingStatusRepository: SynchronisingStatusRepository,
    private val synchronisingService: SynchronisingService,
) : ViewModel() {

    private val failures = MutableLiveData<SynchronisationFailure?>()

    @SuppressLint("CheckResult")
    fun synchronise(lifecycleOwner: LifecycleOwner) {
        synchronisingService.synchronise(lifecycleOwner).subscribe { failure ->
            failures.postValue(failure)
        }
    }

    fun recoverFromFailure() {
        failures.postValue(null)
        synchronisingStatusRepository.clearCache().subscribe()
    }

    fun failures(): LiveData<SynchronisationFailure?> {
        return failures
    }
}
