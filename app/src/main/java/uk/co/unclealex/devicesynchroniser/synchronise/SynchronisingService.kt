package uk.co.unclealex.devicesynchroniser.synchronise

import android.annotation.SuppressLint
import android.content.ContentUris
import android.content.ContentValues
import android.content.Context
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.toPublisher
import androidx.work.OneTimeWorkRequest
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkInfo
import androidx.work.WorkManager
import androidx.work.workDataOf
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.Single.just
import timber.log.Timber
import uk.co.unclealex.devicesynchroniser.AppExecutors
import uk.co.unclealex.devicesynchroniser.api.WebServiceFactory
import uk.co.unclealex.devicesynchroniser.background.FinishSynchronisingWorker
import uk.co.unclealex.devicesynchroniser.background.StartSynchronisingWorker
import uk.co.unclealex.devicesynchroniser.background.SynchronisingWorker
import uk.co.unclealex.devicesynchroniser.db.DeviceDao
import uk.co.unclealex.devicesynchroniser.db.SummaryDao
import uk.co.unclealex.devicesynchroniser.db.SynchronisingStatusDao
import uk.co.unclealex.devicesynchroniser.repository.DeviceRepository
import uk.co.unclealex.devicesynchroniser.repository.SummaryRepository
import uk.co.unclealex.devicesynchroniser.settings.Settings
import uk.co.unclealex.devicesynchroniser.util.Maybes
import uk.co.unclealex.devicesynchroniser.util.toException
import uk.co.unclealex.devicesynchroniser.vo.Delta
import uk.co.unclealex.devicesynchroniser.vo.Summary
import uk.co.unclealex.devicesynchroniser.vo.SynchronisationFailure
import uk.co.unclealex.devicesynchroniser.vo.SynchronisingStatus
import java.io.BufferedInputStream
import java.io.FileOutputStream
import java.net.URL

private const val MAXIMUM_NUMBER_OF_JOBS = 680

class SynchronisingService(
    private val summaryDao: SummaryDao,
    private val deviceDao: DeviceDao,
    private val summaryRepository: SummaryRepository,
    private val deviceRepository: DeviceRepository,
    private val synchronisingStatusDao: SynchronisingStatusDao,
    private val webServiceFactory: WebServiceFactory,
    private val appExecutors: AppExecutors,
    val context: Context,
    val settings: Settings,
) {

    @SuppressLint("EnqueueWork")
    fun synchronise(lifecycleOwner: LifecycleOwner): Maybe<SynchronisationFailure> {
        Log.w("SYNC", "synchronisingService.synchronise")
        val workManager = WorkManager.getInstance(context)
        val flowable =
            synchronisingStatusDao
                .updateStatus(SynchronisingStatus.START)
                .subscribeOn(appExecutors.diskIoScheduler())
                .andThen(prepareSynchronisation())
                .flatMapPublisher { statuses ->
                    val limitedStatuses = statuses.take(MAXIMUM_NUMBER_OF_JOBS)
                    val fullySynchronised = limitedStatuses.size == statuses.size
                    val workRequests =
                        (
                            limitedStatuses.map { sync(it) } +
                                listOf(finish(limitedStatuses, fullySynchronised))
                            )
                    val continuation =
                        workRequests.fold(workManager.beginWith(start())) { workContinuation, workRequest,
                            ->
                            workContinuation.then(workRequest)
                        }
                    continuation.enqueue()
                    continuation.workInfosLiveData.toPublisher(lifecycleOwner)
                }
                .flatMap { workInfos ->
                    Flowable.fromIterable(workInfos)
                        .filter { it.state == WorkInfo.State.FAILED }
                        .flatMap { workInfo ->
                            val failure =
                                workInfo.outputData.toException()?.let {
                                    SynchronisationFailure(it.first, it.second)
                                }
                            Maybes.fromNullable(failure).toFlowable()
                        }
                }
        return flowable.firstElement()
    }

    private fun start(): OneTimeWorkRequest {
        return OneTimeWorkRequestBuilder<StartSynchronisingWorker>().build()
    }

    private fun sync(status: SynchronisingStatus): OneTimeWorkRequest {
        return OneTimeWorkRequestBuilder<SynchronisingWorker>()
            .setInputData(status.toWorkData())
            .build()
    }

    private fun finish(
        statuses: List<SynchronisingStatus>,
        fullySynchronised: Boolean,
    ): OneTimeWorkRequest {
        return OneTimeWorkRequestBuilder<FinishSynchronisingWorker>()
            .setInputData(
                workDataOf(
                    FinishSynchronisingWorker.TOTAL to statuses.size,
                    FinishSynchronisingWorker.FULLY_SYNCHRONISED to fullySynchronised,
                ),
            )
            .build()
    }

    private fun prepareSynchronisation(): Single<List<SynchronisingStatus>> {
        val webService = webServiceFactory.build(settings)
        val deviceIdentifier = settings.getDeviceIdentifier()
        return if (webService == null || deviceIdentifier == null) {
            just(arrayListOf())
        } else {
            val singleDeltas = webService.getDeltas(deviceIdentifier)
            val singleSummaries = summaryDao.maybeFindAll().toSingle(arrayListOf())
            return singleDeltas.flatMap { deltas ->
                singleSummaries.map { summaries -> deltasToSynchronisingStatuses(deltas, summaries) }
            }
        }
    }

    private fun deltasToSynchronisingStatuses(
        deltas: List<Delta>,
        summaries: List<Summary>,
    ): List<SynchronisingStatus> {
        val deltaCount = deltas.size
        val empty: Pair<List<SynchronisingStatus>, Map<String, Int>> =
            arrayListOf<SynchronisingStatus>() to mapOf()
        val statusesAndCounts: Pair<List<SynchronisingStatus>, Map<String, Int>> =
            deltas.foldIndexed(empty) { index, acc, delta ->
                val statuses = acc.first
                val summaryPathCounts = acc.second
                val summaryPath = delta.summaryPath
                val summary =
                    summaryPath?.let { path -> summaries.find { summary -> summary.path == path } }
                val summaryProgress = summaryPath?.let { summaryPathCounts.getOrDefault(it, -1) + 1 }
                val status =
                    SynchronisingStatus(
                        id = SynchronisingStatus.STATIC_ID,
                        delta = delta,
                        summary = summary,
                        last = delta.last ?: false,
                        summaryProgress = summaryProgress,
                        progress = index,
                        total = deltaCount,
                    )
                val newSummaryPathCounts =
                    if (summaryProgress == null && summaryPath == null) {
                        summaryPathCounts
                    } else {
                        summaryPathCounts.plus(summaryPath to summaryProgress!!)
                    }
                statuses.plus(status) to newSummaryPathCounts
            }
        return statusesAndCounts.first
    }

    fun startSynchronising() {
        // Nothing to do
    }

    fun synchronise(synchronisingStatus: SynchronisingStatus) {
        synchronisingStatusDao.updateStatusSync(synchronisingStatus)
        val delta = synchronisingStatus.delta
        val summary = synchronisingStatus.summary
        delta?.let {
            executeDelta(delta)
            updateOffset(delta.ordering)
            summary?.let { cleanUp(summary, delta.last ?: false) }
        }
    }

    private fun updateLastSynchronised() {
        Timber.i("Updating the device last synchronised time")
        val call =
            webServiceFactory.build(settings)!!.updateLastSynchronised(settings.getDeviceIdentifier()!!)
        val deviceResponse = call.execute()
        if (deviceResponse.isSuccessful) {
            Timber.i("Updating the device last synchronised time was successful")
            deviceResponse.body()?.let { device -> deviceDao.updateDeviceSync(device) }
        } else {
            Timber.e("Updating offset failed with error code %d", deviceResponse.code())
        }
    }

    private fun updateOffset(offset: Int) {
        Timber.i("Updating the device offset to %d", offset)
        val call =
            webServiceFactory.build(settings)!!.updateOffset(settings.getDeviceIdentifier()!!, offset)
        val deviceResponse = call.execute()
        if (deviceResponse.isSuccessful) {
            Timber.i("Updating the device offset to %d was successful", offset)
            deviceResponse.body()?.let { device -> deviceDao.updateDeviceSync(device) }
        } else {
            Timber.e("Updating offset failed with error code %d", deviceResponse.code())
        }
    }

    private fun executeDelta(delta: Delta) {
        val path = delta.path.replace('/', '_')
        when (delta.type) {
            Delta.Type.REMOVE_FILE -> {
                remove(path)
            }
            Delta.Type.CREATE_FILE -> {
                createOrUpdate(delta, path)
            }
        }
    }

    private fun collection(): Uri {
        return MediaStore.Audio.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY)
    }

    private fun remove(path: String) {
        val projection = arrayOf(MediaStore.Audio.Media._ID, MediaStore.Audio.Media.DISPLAY_NAME)
        val selection = "${MediaStore.Audio.Media.DISPLAY_NAME} = ?"
        val selectionArgs = arrayOf(path)

        val sortOrder = "${MediaStore.Audio.Media.DISPLAY_NAME} ASC"

        val query = context.contentResolver.query(
            collection(),
            projection,
            selection,
            selectionArgs,
            sortOrder,
        )
        query?.use { cursor ->
            val idColumn = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media._ID)
            val displayNameColumn = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DISPLAY_NAME)
            while (cursor.moveToNext()) {
                val id = cursor.getLong(idColumn)
                val displayName = cursor.getString(displayNameColumn)
                val contentUri: Uri = ContentUris.withAppendedId(
                    MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    id,
                )
                Timber.i("Removing %s", displayName)
                context.contentResolver.delete(contentUri, null, null)
            }
        }
    }

    private fun createOrUpdate(delta: Delta, path: String) {
        remove(path)
        Timber.i("Downloading %s to %s", delta.url, path)
        val songDetails =
            ContentValues().apply {
                put(MediaStore.Audio.Media.DISPLAY_NAME, path)
                put(MediaStore.Audio.Media.IS_PENDING, 1)
            }
        val songContentUri = context.contentResolver.insert(collection(), songDetails)
        context.contentResolver.openFileDescriptor(songContentUri!!, "w", null).use { pfd ->
            val output = FileOutputStream(pfd!!.fileDescriptor)
            BufferedInputStream(URL(delta.url).openStream()).use { source ->
                source.copyTo(output)
            }
        }
        songDetails.clear()
        songDetails.put(MediaStore.Audio.Media.IS_PENDING, 0)
        context.contentResolver.update(songContentUri, songDetails, null, null)
    }

    private fun cleanUp(summary: Summary, last: Boolean) {
        if (last) {
            summaryDao.deleteSync(summary)
        }
    }

    fun finishSynchronising(fullyCompleted: Boolean) {
        if (fullyCompleted) {
            updateLastSynchronised()
        }
        summaryRepository.refresh()
        deviceRepository.refresh()
        synchronisingStatusDao.findByIdSync(SynchronisingStatus.STATIC_ID)?.let { status ->
            synchronisingStatusDao.deleteStatusSync(status)
        }
    }
}
