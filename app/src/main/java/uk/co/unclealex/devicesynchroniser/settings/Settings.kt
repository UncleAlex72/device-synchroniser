package uk.co.unclealex.devicesynchroniser.settings

interface Settings {

    fun getRootSeverUrl(): String?
    fun getDeviceIdentifier(): String?
}
