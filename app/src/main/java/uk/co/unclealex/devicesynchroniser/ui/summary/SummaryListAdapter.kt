/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.co.unclealex.devicesynchroniser.ui.summary

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import uk.co.unclealex.devicesynchroniser.AppExecutors
import uk.co.unclealex.devicesynchroniser.R
import uk.co.unclealex.devicesynchroniser.databinding.SummaryItemBinding
import uk.co.unclealex.devicesynchroniser.vo.Summary

/**
 * A RecyclerView adapter for [Summary.Flat] class.
 */
class SummaryListAdapter(
    private val dataBindingComponent: DataBindingComponent,
    appExecutors: AppExecutors,
    private val repoClickCallback: ((Summary.Flat) -> Unit)?,
) : DataBoundListAdapter<Summary.Flat, SummaryItemBinding>(
    appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<Summary.Flat>() {
        override fun areItemsTheSame(oldItem: Summary.Flat, newItem: Summary.Flat): Boolean {
            return oldItem.ordering == newItem.ordering &&
                oldItem.path == newItem.path &&
                oldItem.removing == newItem.removing
        }

        override fun areContentsTheSame(oldItem: Summary.Flat, newItem: Summary.Flat): Boolean {
            return oldItem.title == newItem.title &&
                oldItem.artist == newItem.artist &&
                oldItem.coverArtUrl == newItem.coverArtUrl
        }
    },
) {

    override fun createBinding(parent: ViewGroup): SummaryItemBinding {
        val binding = DataBindingUtil.inflate<SummaryItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.summary_item,
            parent,
            false,
            dataBindingComponent,
        )
        binding.root.setOnClickListener {
            binding.summary?.let {
                repoClickCallback?.invoke(it)
            }
        }
        return binding
    }

    override fun bind(binding: SummaryItemBinding, item: Summary.Flat) {
        binding.summary = item
    }
}
