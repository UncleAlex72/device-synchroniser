package uk.co.unclealex.devicesynchroniser.api

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import uk.co.unclealex.devicesynchroniser.db.Tokenisable
import uk.co.unclealex.devicesynchroniser.vo.Delta
import uk.co.unclealex.devicesynchroniser.vo.Summary
import java.time.Instant

object DeviceSynchroniserTypeAdapters {

    abstract class NullSafeTypeAdapter<T> : TypeAdapter<T?>() {

        abstract fun nullSafeWrite(writer: JsonWriter, value: T)

        override fun write(out: JsonWriter?, value: T?) {
            if (out != null) {
                if (value != null) {
                    nullSafeWrite(writer = out, value = value)
                } else {
                    out.nullValue()
                }
            }
        }

        abstract fun nullSafeRead(reader: JsonReader): T

        override fun read(`in`: JsonReader?): T? {
            return if (`in` == null) {
                null
            } else {
                val jsonToken = `in`.peek()
                if (jsonToken == JsonToken.NULL) {
                    `in`.nextNull()
                    null
                } else {
                    nullSafeRead(`in`)
                }
            }
        }
    }

    abstract class NullSafeStringTypeAdapter<T> : NullSafeTypeAdapter<T>() {

        abstract fun asString(value: T): String

        override fun nullSafeWrite(writer: JsonWriter, value: T) {
            writer.value(asString(value))
        }

        abstract fun asObject(value: String): T

        override fun nullSafeRead(reader: JsonReader): T {
            return asObject(reader.nextString())
        }
    }

    private fun <T : Tokenisable> tokenisableTypeAdapter(
        name: String,
        values: Array<T>,
    ): TypeAdapter<T?> {
        return object : NullSafeStringTypeAdapter<T>() {
            override fun asString(value: T): String {
                return Tokenisable.asString(value)
            }

            override fun asObject(value: String): T {
                return Tokenisable.asObject(name, values, value)
            }
        }
    }

    val deltaTypeAdapter = tokenisableTypeAdapter("delta action", Delta.Type.values())
    val summaryActionAdapter = tokenisableTypeAdapter("summary action", Summary.Action.values())
    val instantTypeAdapter: TypeAdapter<Instant?> = object : NullSafeStringTypeAdapter<Instant>() {
        override fun asString(value: Instant): String {
            return value.toString()
        }

        override fun asObject(value: String): Instant {
            return Instant.parse(value)
        }
    }
}
