package uk.co.unclealex.devicesynchroniser.db

import java.util.Locale

interface Tokenisable {

    val token: String

    companion object {
        fun <T : Tokenisable> asString(t: T): String {
            return t.token
        }

        fun <T : Tokenisable> asObject(name: String, ts: Array<T>, token: String): T {
            val lowerCaseToken = token.lowercase(Locale.getDefault())
            return ts.find { it.token.lowercase(Locale.getDefault()) == lowerCaseToken }
                ?: throw IllegalArgumentException("$token is not a valid $name")
        }
    }
}
