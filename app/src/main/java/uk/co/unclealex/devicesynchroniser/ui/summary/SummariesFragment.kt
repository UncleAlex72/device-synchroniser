/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.co.unclealex.devicesynchroniser.ui.summary

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import dagger.hilt.android.AndroidEntryPoint
import uk.co.unclealex.devicesynchroniser.AppExecutors
import uk.co.unclealex.devicesynchroniser.R
import uk.co.unclealex.devicesynchroniser.binding.FragmentDataBindingComponent
import uk.co.unclealex.devicesynchroniser.databinding.SummariesFragmentBinding
import uk.co.unclealex.devicesynchroniser.settings.Settings
import uk.co.unclealex.devicesynchroniser.util.autoCleared
import uk.co.unclealex.devicesynchroniser.vo.Resource
import uk.co.unclealex.devicesynchroniser.vo.Status
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import javax.inject.Inject

@AndroidEntryPoint
class SummariesFragment : Fragment() {

    @Inject
    lateinit var appExecutors: AppExecutors

    @Inject
    lateinit var settings: Settings

    var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    var binding by autoCleared<SummariesFragmentBinding>()

    var adapter by autoCleared<SummaryListAdapter>()

    val summariesViewModel: SummariesViewModel by viewModels()

    val canSynchroniseViewModel: CanSynchroniseViewModel by viewModels()

    val deviceViewModel: DeviceViewModel by viewModels()

    val synchroniseViewModel: SynchroniseViewModel by viewModels()

    val synchronisingStatusViewModel: SynchronisingStatusViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.summaries_fragment,
            container,
            false,
            dataBindingComponent,
        )

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.lifecycleOwner = viewLifecycleOwner

        val summaryListAdapter = SummaryListAdapter(
            dataBindingComponent = dataBindingComponent,
            appExecutors = appExecutors,
        ) {}
        adapter = summaryListAdapter
        binding.summariesList.adapter = adapter
        initInnerViews()
    }

    private fun <T> dataOnly(resources: LiveData<Resource<T>>): LiveData<T?> {
        return resources.map { res -> res.data }
    }

    private fun initInnerViews() {
        val swipeRefresh = binding.swipeRefresh
        swipeRefresh.setOnRefreshListener {
            summariesViewModel.refresh()
            deviceViewModel.refresh()
        }

        binding.synchronisingStatus =
            synchronisingStatusViewModel.results.map { status ->
                val flattened = status?.flattened
                status
            }

        binding.summaries = dataOnly(summariesViewModel.results)
        summariesViewModel.results.observe(
            viewLifecycleOwner,
        ) { result ->
            when (result.status) {
                Status.SUCCESS -> {
                    swipeRefresh.isRefreshing = false
                    result.data?.let { adapter.submitList(result.data.map { it.flattened }) }
                }

                Status.REFRESHED -> {
                    swipeRefresh.isRefreshing = false
                }

                else -> {
                    swipeRefresh.isRefreshing = true
                }
            }
        }

        binding.deviceName =
            resourceBinding(deviceViewModel.results, R.string.waiting) { it.displayName }

        binding.lastSynchronised =
            resourceBinding(deviceViewModel.results, R.string.waiting) { device ->
                val lastSynchronised = device.lastSynchronised
                if (lastSynchronised == null) {
                    getString(R.string.never_synchronised)
                } else {
                    val formatter =
                        DateTimeFormatter.ofPattern(getString(R.string.last_synchronised_date_format))
                    lastSynchronised.atZone(ZoneId.systemDefault()).format(formatter)
                }
            }

        binding.changesCount =
            resourceBinding(summariesViewModel.results, R.string.waiting) { results ->
                val count = results.size
                resources.getQuantityString(R.plurals.change_count, count, count)
            }

        binding.showFab = canSynchroniseViewModel.results

        binding.fab.setOnClickListener {
            synchroniseViewModel.synchronise(this)
        }

        synchroniseViewModel.failures()
            .observe(
                viewLifecycleOwner,
            ) { synchronisationFailure ->
                synchronisationFailure?.let {
                    val message = "${it.exceptionClass}: ${it.message ?: "No Message"}"
                    AlertDialog.Builder(context).setMessage(message).setPositiveButton(
                        R.string.ok,
                    ) { _, _ ->
                        synchroniseViewModel.recoverFromFailure()
                    }.create().show()
                }
            }
    }

    fun <S> resourceBinding(
        source: LiveData<Resource<S>>,
        waiting: Int,
        success: (S) -> String,
    ): LiveData<String> {
        return source.map { resource ->
            resource.data?.let { success(it) } ?: getString(waiting)
        }
    }
}
